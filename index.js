module.exports = {
    add: (a, b) => { return a + b },
    minus: (a, b) => { return (a - b) },
    multi: (a, b) => { return (a * b) },
    divide: (a, b) => { 

        if(b == 0) throw new Error()

        return (a / b) 
    
    }
}